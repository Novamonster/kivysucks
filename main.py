#!/usr/bin/env python

##To run for tests:
# python3 main.py -m screen:phone_iphone_6 #,portrait

##Imports -- Kivy
import kivy
from kivy.app import App

from kivy.uix.gridlayout import GridLayout
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget

from kivy.graphics import Ellipse

from kivy.core.audio import SoundLoader
from kivy.core.window import Window

from kivy.lang import Builder
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty

from kivy.vector import Vector
from kivy.clock import Clock

##Imports -- Py
import gc
import json
import os
import time

##Imports -- Project
from pyToKivy2 import PythonToKivy
from libraries import Library

##Configuration
kivy.require("1.11.1")
# os.environ['KIVY_TEXT'] = 'pil'
# os.environ["KCFG_KIVY_LOG_LEVEL"] = "warning"

##Hybrid Use Classes:
class NameInput(TextInput):
    """docstring for NameInput"""
    def __init__(self, **kwargs):
        super(NameInput, self).__init__(**kwargs)

    def insert_text(self, substring, from_undo=True):
        s = str(substring)
        if len(s)>12:
            s = s[:12]
        return super(NameInput, self).insert_text(s, from_undo=from_undo)

##Screens:
class Harambe(Screen):
    """docstring for sad Harambe"""
    def __init__(self, **kwargs):
        super(Harambe, self).__init__(**kwargs)
        self.number = 1



    def update(self):
        dictionary = {}    
        self.ids.meme.text = str(self.number)
        self.number += 1
        self.ids.meme2.text = str(self.number)
        self.number += 1
        self.ids.meme3.text = str(self.number) 
        self.number += 1
        dictionary['one'] = self.ids.meme.text
        dictionary['two'] = self.ids.meme2.text
        dictionary['three'] = self.ids.meme3.text

        print(dictionary)

    def clear(self):
        self.ids.meme.text = str(self.manager.library.data_model.clear())
        self.ids.meme2.text = str(self.manager.library.data_model.clear())
        self.ids.meme3.text = str(self.manager.library.data_model.clear())
        self.manager.library.increase()

    def clear2(self):
        self.manager.library.decrease()


#Main App Units:
class WindowManager(ScreenManager):
    tfm = PythonToKivy()
    library = tfm(Library())
        


class PongApp(App):
    """Main launch point for the Critter app."""
    def build(self):
        self.root = Builder.load_file("./critter2.kv")
        Window = WindowManager()
        Window.add_widget(Harambe(name='Screen1'))
        return Window

if __name__ == '__main__':
    PongApp().run()
